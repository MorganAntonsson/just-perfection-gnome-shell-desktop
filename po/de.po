# Just Perfection GNOME Shell Extension Translation
# Copyright (C) 2020
# This file is distributed under GPL v3
# Just Perfection <justperfection.channel@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-22 11:19-0700\n"
"PO-Revision-Date: 2020-12-18 04:55-0800\n"
"Last-Translator: Just Perfection <justperfection.channel@gmail.com>\n"
"Language-Team: German\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ui/prefs.ui:73
msgid "Support"
msgstr "Unterstützung"

#: ui/prefs.ui:87
msgid ""
"Your support helps the development process continue and brings more features "
"to the extension"
msgstr ""
"Ihr Support hilft dabei, den Entwicklungsprozess fortzusetzen, und bringt "
"weitere Funktionen in die Erweiterung"

#: ui/prefs.ui:298
msgid ""
"You can support my work via Patreon. Also your name will be mentioned at the "
"end of videos on my YouTube Channel as Patreon supporter."
msgstr ""
"Sie können meine Arbeit über Patreon unterstützen. Außerdem wird dein Name "
"am Ende der Videos auf meinem YouTube-Kanal als Patreon-Unterstützer erwähnt."

#: ui/prefs.ui:457
msgid "No Results Found"
msgstr "Keine Ergebnisse gefunden"

#: ui/prefs.ui:479
msgid "Override"
msgstr "Überschreiben"

#: ui/prefs.ui:536
msgid "Shell Theme"
msgstr "Shell-Thema"

#: ui/prefs.ui:547
msgid "Overrides the shell theme partially to create a minimal desktop"
msgstr ""
"Überschreibt das Shell-Design teilweise, um einen minimalen Desktop zu "
"erstellen"

#: ui/prefs.ui:597
msgid "Visibility"
msgstr "Sichtbarkeit"

#: ui/prefs.ui:646
msgid "Panel"
msgstr "Obere Leiste"

#: ui/prefs.ui:695
msgid "Activities Button"
msgstr "Die Aktivitäten Taste"

#: ui/prefs.ui:744
msgid "App Menu"
msgstr "Das Anwendungsmenü"

#: ui/prefs.ui:793
msgid "Clock Menu"
msgstr "Uhr Menü"

#: ui/prefs.ui:842
msgid "Keyboard Layout"
msgstr "Tastaturbelegung"

#: ui/prefs.ui:891
msgid "Accessibility Menu"
msgstr "Eingabehilfen-Menü"

#: ui/prefs.ui:940
msgid "System Menu (Aggregate Menu)"
msgstr "Das Systemmenü"

#: ui/prefs.ui:989
msgid "Search"
msgstr "Suche"

#: ui/prefs.ui:1038
msgid "Dash"
msgstr "Dash"

#: ui/prefs.ui:1087
msgid "Show Applications Button"
msgstr "Schaltfläche Anwendungen anzeigen"

#: ui/prefs.ui:1136
msgid "On Screen Display (OSD)"
msgstr "OSD"

#: ui/prefs.ui:1185
msgid "Workspace Popup"
msgstr "Arbeitsflächen-Popup"

#: ui/prefs.ui:1234
msgid "Workspace Switcher"
msgstr "Arbeitsflächenumschalter"

#: ui/prefs.ui:1283
msgid "Background Menu"
msgstr "Hintergrundmenü"

#: ui/prefs.ui:1324
msgid "Icons"
msgstr "Symbole"

#: ui/prefs.ui:1373
msgid "App Menu Icon"
msgstr "Das Anwendungsmenüsymbol"

#: ui/prefs.ui:1422
msgid "Panel Notification Icon"
msgstr "Panel-Benachrichtigungssymbol"

#: ui/prefs.ui:1471
msgid "Power Icon"
msgstr "Power Icon"

#: ui/prefs.ui:1520
msgid "Panel Arrow"
msgstr "Bedienfeldpfeil"

#: ui/prefs.ui:1569
msgid "Window Picker Icon"
msgstr "Fensterauswahl-Symbol"

#: ui/prefs.ui:1610
msgid "Behavior"
msgstr "Verhalten"

#: ui/prefs.ui:1659
msgid "Hot Corner"
msgstr "Funktionale Ecke"

#: ui/prefs.ui:1708
msgid "App Gesture"
msgstr "App-Geste"

#: ui/prefs.ui:1763
msgid "Type to Search"
msgstr "Tippe um zu suchen"

#: ui/prefs.ui:1774
msgid ""
"You can start search without search entry or even focusing on it in overview"
msgstr ""
"Sie können die Suche starten, ohne einen Sucheintrag zu haben oder sich in "
"der Übersicht darauf zu konzentrieren"

#: ui/prefs.ui:1824
msgid "Customize"
msgstr "Anpassen"

#: ui/prefs.ui:1872
msgid "Panel Corner Size"
msgstr "Panel Corner Größe"

#: ui/prefs.ui:1883
msgid "By Shell Theme"
msgstr "Nach Shell-Thema"

#: ui/prefs.ui:1884
msgid "No Corner"
msgstr "Keine Ecke"

#: ui/prefs.ui:1984
msgid "Panel Position"
msgstr "Panel Position"

#: ui/prefs.ui:1995
msgid "Top"
msgstr "oben"

#: ui/prefs.ui:1996
msgid "Bottom"
msgstr "Unterseite"

#: ui/prefs.ui:2036
msgid "Clock Menu Position"
msgstr "Position des Uhrmenüs"

#: ui/prefs.ui:2047
msgid "Center"
msgstr "Center"

#: ui/prefs.ui:2048
msgid "Right"
msgstr "Richtig"

#: ui/prefs.ui:2049
msgid "Left"
msgstr "Links"

#: ui/prefs.ui:2089
msgid "Clock Menu Position Offset"
msgstr "Positionsversatz des Uhrmenüs"

#: ui/prefs.ui:2149
msgid "Workspace Switcher Size"
msgstr "Größe des Arbeitsbereichsumschalters"

#: ui/prefs.ui:2160
msgid "Default"
msgstr "Standard"

#: ui/prefs.ui:2210
msgid "Animation"
msgstr "Animation"

#: ui/prefs.ui:2221
msgid "No Animation"
msgstr "Keine Animation "

#: ui/prefs.ui:2222
msgid "Default Speed"
msgstr "Standardgeschwindigkeit"

#: ui/prefs.ui:2223
msgid "Fastest"
msgstr "Am schnellsten"

#: ui/prefs.ui:2224
msgid "Faster"
msgstr "Schneller"

#: ui/prefs.ui:2225
msgid "Fast"
msgstr "Schnell"

#: ui/prefs.ui:2226
msgid "Slow"
msgstr "Schleppend"

#: ui/prefs.ui:2227
msgid "Slower"
msgstr "Langsamer"

#: ui/prefs.ui:2228
msgid "Slowest"
msgstr "Am langsamsten"
